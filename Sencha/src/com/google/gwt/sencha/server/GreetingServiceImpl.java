package com.google.gwt.sencha.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

import com.google.gwt.sencha.client.GreetingService;
import com.google.gwt.sencha.client.User;
import com.google.gwt.sencha.shared.FieldVerifier;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The server-side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements GreetingService {
	
	private ArrayList<User> userList = new ArrayList<User>();
	
	public String greetServer(String input) throws IllegalArgumentException {
		// Verify that the input is valid. 
		if (!FieldVerifier.isValidName(input)) {
			// If the input is not valid, throw an IllegalArgumentException back to
			// the client.
			throw new IllegalArgumentException("Name must be at least 4 characters long");
		}

		String serverInfo = getServletContext().getServerInfo();
		String userAgent = getThreadLocalRequest().getHeader("User-Agent");

		// Escape data from the client to avoid cross-site script vulnerabilities.
		input = escapeHtml(input);
		userAgent = escapeHtml(userAgent);

		return "Hello, " + input + "!<br><br>I am running " + serverInfo + ".<br><br>It looks like you are using:<br>"
				+ userAgent;
	}

	/**
	 * Escape an html string. Escaping data received from the client helps to
	 * prevent cross-site script vulnerabilities.
	 * 
	 * @param html the html string to escape
	 * @return the escaped string
	 */
	private String escapeHtml(String html) {
		if (html == null) {
			return null;
		}
		return html.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;");
	}
	public static void writeInFile(User user) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter("userList.txt", true));
		writer.append("{ first name: " + user.getFirstName() + " last name: " + user.getLastName() + " sex:" +
		user.getSex() + " age: " + user.getAge() + " contry: " + user.getCountry() + " is married: " + 
				user.getIsMarried() + " birthday: " + user.getDate() + " email: " + user.getEmail() + 
				" number: " + user.getNumber() + "}");
		writer.newLine();
		writer.close();
	}
	static void removeFromFile(User user) throws IOException {
    	File file = new File("userList.txt");
    	
    	String delete = "{ first name: " + user.getFirstName() + " last name: " + user.getLastName() + " sex:" +
		user.getSex() + " age: " + user.getAge() + " contry: " + user.getCountry() + " is married: " + 
				user.getIsMarried() + " birthday: " + user.getDate() + " email: " + user.getEmail() + 
				" number: " + user.getNumber() + "}";
    	File temp = File.createTempFile("file", ".txt", file.getParentFile());
    	
    	BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
    	
    	PrintWriter writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(temp), "UTF-8"));
    	
    	for (String line; (line = reader.readLine()) != null;) {
    		 line = line.replace(delete, "");
    		 writer.println(line);
    	}
    	reader.close();
    	writer.close();
    	file.delete();
    	temp.renameTo(file);
	}

	@Override
	public User createUser(User user) throws IOException {
		userList.add(user);
		writeInFile(user);
		return user;
	}

	@Override
	public String deleteUser(User user) throws IOException {
		userList.remove(user);
		removeFromFile(user);
		return "";
	}

	@Override
	public String updateUser(User oldUser, User newUser) throws IOException {
//		File file = new File("userList.txt");
//		try {
//			File temp = File.createTempFile("userInfo", ".txt", file.getParentFile());
//			String charset = "UTF-8";
//			String delete = "{ first name: " + oldUser.getFirstName() + " last name: " + oldUser.getLastName() + " sex:" +
//					oldUser.getSex() + " age: " + oldUser.getAge() + " contry: " + oldUser.getCountry() + " is married: " + 
//							oldUser.getIsMarried() + " birthday: " + oldUser.getDate() + "}";
//			    	String replace = "{ first name: " + newUser.getFirstName() + " last name: " + newUser.getLastName() + " sex:" +
//					newUser.getSex() + " age: " + newUser.getAge() + " contry: " + newUser.getCountry() + " is married: " + 
//							newUser.getIsMarried() + " birthday: " + newUser.getDate() + "}";
//			    	BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), charset));
//					PrintWriter writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(temp), charset));
//					for (String line; (line = reader.readLine()) != null;) {
//						line = line.replace(delete, replace);
//						writer.println(line);
//					}
//					reader.close();
//					writer.close();
//					file.delete();
//					temp.renameTo(file);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		removeFromFile(oldUser);
		createUser(newUser);
		return "";
	}
}
