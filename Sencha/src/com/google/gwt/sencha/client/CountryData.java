package com.google.gwt.sencha.client;

import java.util.List;
import java.util.ArrayList;

public class CountryData 
{
    public static List<Country> getCountries()
    {
        List<Country> countries = new ArrayList<Country>();
        countries.add(new Country("uk", "United Kingdom"             ));
        countries.add(new Country("ro", "Romania"      ));
        countries.add(new Country("ukr", "Ukraine"));
        countries.add(new Country("us", "USA"           ));
        countries.add(new Country("th", "Thailand"            ));
        countries.add(new Country("sp", "Spain"            ));
        return countries;
    }
}