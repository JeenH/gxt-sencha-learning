package com.google.gwt.sencha.client;

import com.google.gwt.editor.client.Editor.Path;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

public interface UserProperties extends PropertyAccess<User> {
	@Path("id")
	ModelKeyProvider<User> key();
	
	@Path("firstName")
	ValueProvider<User, String> firstName();
	
	@Path("lastName")
	ValueProvider<User, String> lastName();
	
	@Path("date")
	ValueProvider<User, String> date();
}
