package com.google.gwt.sencha.client;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.EditorError;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell.TriggerAction;
import com.sencha.gxt.core.client.util.Format;
import com.sencha.gxt.core.client.util.ToggleGroup;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.Dialog.PredefinedButton;
import com.sencha.gxt.widget.core.client.box.ConfirmMessageBox;
import com.sencha.gxt.widget.core.client.box.MessageBox;
import com.sencha.gxt.widget.core.client.box.PromptMessageBox;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.DialogHideEvent;
import com.sencha.gxt.widget.core.client.event.DialogHideEvent.DialogHideHandler;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.CheckBox;
import com.sencha.gxt.widget.core.client.form.DateField;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.Radio;
import com.sencha.gxt.widget.core.client.form.ShortSpinnerField;
import com.sencha.gxt.widget.core.client.form.StringComboBox;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.form.Validator;
import com.sencha.gxt.widget.core.client.form.validator.RegExValidator;
import com.sencha.gxt.widget.core.client.info.Info;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;


public class ContentContainerPanel implements IsWidget{

	public static final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	
	public static ListStore<User> userStore = new ListStore<User>(PersonDataContainer.properties.key());
	private TextButton saveButton = new TextButton("Save");
	private TextButton cancelButton = new TextButton("Cancel");
	private Radio maleButton = new Radio();
	private Radio femaleButton = new Radio();
	private ShortSpinnerField ageSpinnerField = new ShortSpinnerField();
	private TextField firstNameTxtField = new TextField();
	private TextField lastNameTxtField = new TextField();
	private StringComboBox combo1 = null;
	private CheckBox checkBox = new CheckBox();
	private DateField date = new DateField();
    private String sexValue;
	private VerticalLayoutContainer vlc;
	private TextField idTextField = new TextField();
	private TextButton editButton = new TextButton("Edit");
	private TextField emailTxtField = new TextField();
	private TextField numberTxtField = new TextField();
	public int id = 0;
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public Widget asWidget() {
		
		if (vlc == null) {
			
			vlc = new VerticalLayoutContainer();
			User user = new User();
			vlc.add(new FieldLabel(getFirstTxtField(),"First Name"));
			vlc.add(new FieldLabel(getLastTxtField(), "Last name"));
			ToggleGroup toggle = new ToggleGroup();
			toggle.add(getMaleButton());
			toggle.add(getFemaleButton());
			HorizontalPanel horizontalPanel = new HorizontalPanel();
			horizontalPanel.add(getMaleButton());
			horizontalPanel.add(getFemaleButton());
			vlc.add(new FieldLabel(horizontalPanel, "Sex"));
			vlc.add(new FieldLabel(getAgeField(), "Age"));
			vlc.add(new FieldLabel(getCountryCombobox(), "Country"));
			vlc.add(new FieldLabel(getCheckBox(), "Married?"));
			vlc.add(new FieldLabel(getDateField(), "Birthday"));
			vlc.add(new FieldLabel(getEmailTxtField(), "Email"));
			vlc.add(new FieldLabel(getNumberTxtField(), "Number"));
			vlc.add(getIdTextField());
			idTextField.setVisible(false);


		}
		return vlc;
	}

	public TextField getIdTextField() {
		if (idTextField == null) {
			idTextField = new TextField();
		}
		return idTextField;
	}

	public TextButton getSaveButton() {
		if (saveButton == null) {
			saveButton = new TextButton("Save");
		}
		return saveButton;
	}

	public HorizontalPanel getButtonPanel() {
		HorizontalPanel hPanel = new HorizontalPanel();

		hPanel.add(saveButton);
		hPanel.add(cancelButton);
		cancelButton.addHandler(new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				getFirstTxtField().setText("");
				getLastTxtField().setText("");
				getCountryCombobox().setValue("");
				getDateField().setValue(null);
			}
			
		}, SelectEvent.getType());
		saveButton.addHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				if (!getFirstTxtField().getText().matches("[A-Z][a-z]*")) {
					getFirstTxtField().selectAll();
					getFirstTxtField().markInvalid("Only letters are allowed");
					return;
				}
				if (!getLastTxtField().getText().matches("[A-Z][a-z]*")) {
					getLastTxtField().markInvalid("Field should start with big letter. Only letters are allowed");
					getLastTxtField().selectAll();
					return;
				}
				if (!getNumberTxtField().getText().matches("-?\\d+(\\.\\d+)?")) {
					getNumberTxtField().markInvalid("It is the numeric field");
					getNumberTxtField().selectAll();
					return;
				}
				if (!getEmailTxtField().getText().matches("^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$")) {
					getEmailTxtField().markInvalid("Filed should contain [letters,@, letters,.,letters]");
					getEmailTxtField().selectAll();
					return;
				}
				if (maleButton.getValue()) {
					sexValue = "Male";
				} else {
					sexValue = "Female";
				}
				
				userStore.commitChanges();
				User user = new User();
				user.setId(id);
				user.setAge(getAgeField().getText());
				user.setFirstName(getFirstTxtField().getText());
				user.setLastName(getLastTxtField().getText());
				user.setSex(sexValue);
				user.setCountry(getCountryCombobox().getValue());
				if (getCheckBox().getValue()) {
					user.setIsMarried(true);
				} else {
					user.setIsMarried(false);
				}
				user.setDate(getDateField().getText());
				user.setEmail(getEmailTxtField().getText());
				user.setNumber(getNumberTxtField().getText());
				try {
					setNewUser(user);
				} catch (IOException e) {
					
					e.printStackTrace();
				}
				getFirstTxtField().setText("");
				getLastTxtField().setText("");
				emailTxtField.setEmptyText("");
			    getCountryCombobox().setEmptyText("Select a country...");
			    numberTxtField.setEmptyText("");
				getDateField().setValue(null);
			}
			
		}, SelectEvent.getType());
		
		return hPanel;
	}
	public void setNewUser(User user) throws IOException {
		greetingService.createUser(user, new AsyncCallback<User>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert(caught.toString());
				
			}

			@Override
			public void onSuccess(User result) {
				
			}
			
		});
			id++;
			userStore.add(user);
			
	}
	public void removeFromStore(int id) {
		userStore.remove(id);
	}
	public Radio getMaleButton() {
		
		maleButton = new Radio();
		maleButton.setBoxLabel("Male");
	
		return maleButton;
	}
	public Radio getFemaleButton() {
	
		femaleButton = new Radio();
		femaleButton.setBoxLabel("Female");
	
		return femaleButton;
	}

	public ShortSpinnerField getAgeField() {
		ageSpinnerField.setValue(Short.parseShort("19"));
	
		if (ageSpinnerField == null) {
			ageSpinnerField = new ShortSpinnerField();
			ageSpinnerField.setAllowNegative(false);
	
		}
		return ageSpinnerField;
	}
	
	public TextField getFirstTxtField() {
		if (firstNameTxtField == null) {
			firstNameTxtField = new TextField();
		}
		return firstNameTxtField;
	}
	public TextField getLastTxtField() {
		if (lastNameTxtField == null) {
			lastNameTxtField = new TextField();
		}
		return lastNameTxtField;
	}
	public TextField getEmailTxtField() {
		emailTxtField.setEmptyText("example@mail.com");
		if (emailTxtField == null) {
			emailTxtField = new TextField();
		}
		return emailTxtField;
	}
	public TextField getNumberTxtField() {
		numberTxtField.setEmptyText("0123456789");
		if (numberTxtField == null) {
			numberTxtField = new TextField();
		}
		return numberTxtField;
	}
	public StringComboBox getCountryCombobox()
	{
	    combo1 = new StringComboBox();
	    combo1.add("Ukraine");
	    combo1.add("USA");
	    combo1.add("Romania");
	    combo1.add("Thailand");
	    
	    combo1.setEmptyText("Select a country...");
	    combo1.setName("combo1");
	    combo1.setTypeAhead(true);
	    combo1.setTriggerAction(TriggerAction.ALL);
	    combo1.setWidth(180);
		return combo1;
	}
	
	public CheckBox getCheckBox() {
		if (checkBox == null) {
			checkBox = new CheckBox();
	    checkBox.setTitle("GXT CheckBox");
	    checkBox.setBoxLabel("BoxLabel");
	    checkBox.setOriginalValue(true);
		}
	    return checkBox;
	  }
	
	public DateField getDateField() {
		if (date == null) {
	     date = new DateField();
		}
	   return date;
	  }
	

	public void setFields(String firstName, String lastName, String age, String sex) {
		firstNameTxtField.setText(firstName);
		lastNameTxtField.setText(lastName);
		ageSpinnerField.setValue(Short.parseShort(age));
		if (sex == "Male") {
			maleButton.setValue(true);
		} else if (sex == "Female") {
			femaleButton.setValue(true);
		}
	}
	public TextButton getEditButton() {
		if (editButton == null) {
			editButton = new TextButton();
		}
		return editButton;
	}	
    private TextButton deleteButton = new TextButton("Delete");

    public TextButton getDeleteButton() {
    	if (deleteButton == null) {
    		deleteButton = new TextButton();
    	}
    	return deleteButton;
    }
	

}
