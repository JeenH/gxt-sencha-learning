package com.google.gwt.sencha.client;

import java.io.Serializable;

public class User implements Serializable{
	private int id;
	private String firstName;
	private String lastName;
	private String sex;
	private String age;
	private String country;
	private Boolean isMarried;
	private String date;
	private String email;
	private String number;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Boolean getIsMarried() {
		return isMarried;
	}
	public void setIsMarried(Boolean isHuman) {
		this.isMarried = isHuman;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	
	public User(int id, String firstName, String lastName, String sex, String age, String country, Boolean isMarried,
			String date, String email, String number) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.sex = sex;
		this.age = age;
		this.country = country;
		this.isMarried = isMarried;
		this.date = date;
		this.email = email;
		this.number = number;
	}
	public User() {}
	
}
