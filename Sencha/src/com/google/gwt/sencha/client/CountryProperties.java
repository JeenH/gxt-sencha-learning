package com.google.gwt.sencha.client;

import com.google.gwt.editor.client.Editor.Path;

import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

public interface CountryProperties extends PropertyAccess<Country>
{
    @Path("abbr")
    ModelKeyProvider<Country> abbr();
  
    @Path("name")
    LabelProvider<Country> name();
}