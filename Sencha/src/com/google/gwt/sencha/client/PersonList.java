package com.google.gwt.sencha.client;

import java.util.List;

public class PersonList {
	private List<User> userList;

	public List<User> getUserList() {
		return userList;
	}

	public void addUsers(User user) {
		this.userList.add(user);
	}
	
	
}
