package com.google.gwt.sencha.client;

public class Country {
    private String abbr ;
    private String name ;

    public Country() {}

	public Country(String abbr, String name) {
		this.abbr = abbr;
		this.name = name;
	}

	public String getAbbr() {
		return abbr;
	}

	public void setAbbr(String abbr) {
		this.abbr = abbr;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

    
}
