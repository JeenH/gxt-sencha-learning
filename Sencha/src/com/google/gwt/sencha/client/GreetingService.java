package com.google.gwt.sencha.client;

import java.io.IOException;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("greet")
public interface GreetingService extends RemoteService {
	String greetServer(String name) throws IllegalArgumentException;
	User createUser(User user)  throws IOException;
	String deleteUser(User user) throws IOException;
	String updateUser(User oldUser, User newUser) throws IOException;
}
