package com.google.gwt.sencha.client;

import java.io.IOException;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface GreetingServiceAsync {
	void greetServer(String input, AsyncCallback<String> callback) throws IllegalArgumentException;
	void createUser(User user, AsyncCallback<User> callback)  throws IOException;
	void deleteUser(User user, AsyncCallback<String> callback) throws IOException;
	void updateUser(User oldUser, User newUser, AsyncCallback<String> callback) throws IOException;
}
