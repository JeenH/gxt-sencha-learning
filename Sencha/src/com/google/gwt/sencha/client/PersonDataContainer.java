package com.google.gwt.sencha.client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.Style.SelectionMode;
import com.sencha.gxt.core.client.util.Format;
import com.sencha.gxt.data.shared.event.StoreDataChangeEvent;
import com.sencha.gxt.data.shared.event.StoreDataChangeEvent.StoreDataChangeHandler;
import com.sencha.gxt.state.client.GridStateHandler;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.info.Info;
import com.sencha.gxt.widget.core.client.Dialog.PredefinedButton;
import com.sencha.gxt.widget.core.client.box.MessageBox;
import com.sencha.gxt.widget.core.client.event.DialogHideEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.DialogHideEvent.DialogHideHandler;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.selection.SelectionChangedEvent;
import com.sencha.gxt.widget.core.client.selection.SelectionChangedEvent.SelectionChangedHandler;


public class PersonDataContainer implements IsWidget{
	

	public static UserProperties properties = GWT.create(UserProperties.class);
	private HorizontalPanel hPanel;
	private User person = new User();
	private ContentContainerPanel contentPanel= new ContentContainerPanel(); 
	
	@Override
	public Widget asWidget() {
		
		if (hPanel == null) {
			hPanel = new HorizontalPanel();
			hPanel.add(getGrid());
			hPanel.add(getContentPanel());
			hPanel.add(contentPanel.getEditButton());
			hPanel.add(contentPanel.getDeleteButton());
			
		}
		hPanel.setSpacing(7);
		contentPanel.getSaveButton().setVisible(true);
		return hPanel;
	}

	public ContentContainerPanel getContentPanel() {
		 if(contentPanel != null) {
		 contentPanel  = new ContentContainerPanel();
		}
		return contentPanel;
		}
	public Grid<User> getGrid() {
	      ColumnConfig<User, String> nameCol = new ColumnConfig<User, String>(properties.firstName(), 50, "Username");
	      ColumnConfig<User, String> lastNameCol = new ColumnConfig<User, String>(properties.lastName(), 50, "Last Name");
	      ColumnConfig<User, String> birthdayCol = new ColumnConfig<User, String>(properties.date(), 50, "Birthday");

	      List<ColumnConfig<User, ?>> columns = new ArrayList<ColumnConfig<User, ?>>();
	      columns.add(nameCol);
	      columns.add(lastNameCol);
	      columns.add(birthdayCol);

	      ColumnModel<User> cm = new ColumnModel<User>(columns);    
	      
	      final Grid<User> grid = new Grid<User>(ContentContainerPanel.userStore, cm);
	      grid.setAllowTextSelection(false);
	      grid.getView().setAutoExpandColumn(nameCol);
	      grid.getView().setStripeRows(true);
	      grid.getView().setColumnLines(true);
	      grid.setBorders(false);
	      grid.setColumnReordering(true);


	      // Stage manager, turn on state management
	      grid.setStateful(true);
	      grid.setStateId("gridExample");
	      
	      // Stage manager, load previous state
	      GridStateHandler<User> state = new GridStateHandler<User>(grid);
	      state.loadState();
	      
	      grid.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
	      grid.getSelectionModel().addSelectionChangedHandler(new SelectionChangedHandler<User>() {

			@Override
			public void onSelectionChanged(SelectionChangedEvent<User> user)  {
				if (user.getSelection().size() > 0) {
					User oldUser = new User();
					User newUser = new User();
					
					contentPanel.getFirstTxtField().setText(user.getSelection().get(0).getFirstName());
					contentPanel.getLastTxtField().setText(user.getSelection().get(0).getLastName());
					if (user.getSelection().get(0).getSex().equals("Male")) {
						contentPanel.getMaleButton().setValue(true);
						contentPanel.getFemaleButton().setValue(false);
					} else if (user.getSelection().get(0).getSex().equals("Female")) {
						contentPanel.getFemaleButton().setValue(true);
						contentPanel.getMaleButton().setValue(false);
					}
					contentPanel.getAgeField().setValue(Short.parseShort(user.getSelection().get(0).getAge()));
					contentPanel.getDateField().setText(user.getSelection().get(0).getDate());
					contentPanel.getCountryCombobox().setValue(user.getSelection().get(0).getCountry());
					Integer personId = user.getSelection().get(0).getId();
					contentPanel.getIdTextField().setText(personId.toString());
					oldUser.setFirstName(user.getSelection().get(0).getFirstName());
					oldUser.setLastName(user.getSelection().get(0).getLastName());
					oldUser.setAge(user.getSelection().get(0).getAge());
					if (user.getSelection().get(0).getSex().equals("Male")) {
						contentPanel.getMaleButton().setValue(true);
						oldUser.setSex("Male");
						contentPanel.getFemaleButton().setValue(false);
					} else if (user.getSelection().get(0).getSex().equals("Female")) {
						contentPanel.getFemaleButton().setValue(true);
						contentPanel.getMaleButton().setValue(false);
						oldUser.setSex("Female");
					}
					oldUser.setDate(user.getSelection().get(0).getCountry());
					oldUser.setCountry(user.getSelection().get(0).getCountry());
					contentPanel.getEditButton().addSelectHandler(new SelectHandler() {

						@Override
						public void onSelect(SelectEvent event) {
							user.getSelection().get(0).setFirstName(contentPanel.getFirstTxtField().getText());
							user.getSelection().get(0).setLastName(contentPanel.getLastTxtField().getText());
							user.getSelection().get(0).setDate(contentPanel.getDateField().getText());
							user.getSelection().get(0).setCountry(contentPanel.getCountryCombobox().getValue());
							user.getSelection().get(0).setAge(contentPanel.getAgeField().getText());
							if (contentPanel.getMaleButton().getValue()) {
								user.getSelection().get(0).setSex("Male");
							} else if (contentPanel.getFemaleButton().getValue()) {
								user.getSelection().get(0).setSex("Female");
							}
							
							newUser.setFirstName(contentPanel.getFirstTxtField().getText());
							newUser.setLastName(contentPanel.getLastTxtField().getText());
							newUser.setDate(contentPanel.getDateField().getText());
							newUser.setCountry(contentPanel.getCountryCombobox().getValue());
							newUser.setAge(contentPanel.getAgeField().getText());
							if (contentPanel.getMaleButton().getValue()) {
								user.getSelection().get(0).setSex("Male");
								newUser.setSex("Male");
							} else if (contentPanel.getFemaleButton().getValue()) {
								user.getSelection().get(0).setSex("Female");
								newUser.setSex("Female");
							}
							grid.getStore().update(user.getSelection().get(0));
							try {
								ContentContainerPanel.greetingService.deleteUser(oldUser, new AsyncCallback<String>() {
									
									@Override
									public void onSuccess(String result) {
										// TODO Auto-generated method stub
										
									}
									
									@Override
									public void onFailure(Throwable caught) {
										// TODO Auto-generated method stub
										
									}
								});
								ContentContainerPanel.greetingService.createUser(newUser, new AsyncCallback<User>() {

									@Override
									public void onFailure(Throwable caught) {
										Window.alert(caught.toString());
									}

									@Override
									public void onSuccess(User result) {
										
									}
									
								});
							} catch (IOException e) {
								
								e.printStackTrace();
							}
						}
						
					});
					contentPanel.getDeleteButton().addSelectHandler(new SelectHandler() {

						@Override
						public void onSelect(SelectEvent event) {
					        MessageBox messageBox = new MessageBox("Message Box � Yes/No/Cancel", "");
					        messageBox.setPredefinedButtons(PredefinedButton.YES, PredefinedButton.NO, PredefinedButton.CANCEL);
					        messageBox.setIcon(MessageBox.ICONS.question());
					        messageBox.setMessage("Are you sure you want to delete user '" + contentPanel.getFirstTxtField().getText() + "'?");
					        messageBox.addDialogHideHandler(new DialogHideHandler() {
					          @Override
					          public void onDialogHide(DialogHideEvent event) {
					            String msg = Format.substitute("The '{0}' button was pressed", event.getHideButton());

					            switch (event.getHideButton()) {
					              case YES:
					                Info.display("MessageBox", "Yes: " + msg);	
					        		try {
										ContentContainerPanel.greetingService.deleteUser(user.getSelection().get(0), new AsyncCallback<String>() {
											@Override
											public void onFailure(Throwable caught) {
												Window.alert(caught.toString());
											}

											@Override
											public void onSuccess(String result) {
												// TODO Auto-generated method stub
												
											}
										});
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
					                grid.getStore().remove(user.getSelection().get(0));
					                break;
					              case NO:
					                Info.display("MessageBox", "No: " + msg);
					                break;
					              case CANCEL:
					                Info.display("MessageBox", "Canceled: " + msg);
					                break;
					            }
					          }
					        });
					        messageBox.show();
						}
						
					});
			}
			}
	    	  
	      });

	      grid.setWidth("300px");
	      grid.setHeight(100);
	      return grid;

	}
}
