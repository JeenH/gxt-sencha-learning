package com.google.gwt.sencha.client;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.TabPanel;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;

public class GeneralContainer implements IsWidget{
	private ContentPanel panel;
	
	private ContentContainerPanel contentPanel = new ContentContainerPanel();
	private PersonDataContainer personPanel = new PersonDataContainer();

	@Override
	public Widget asWidget() {
		if (panel == null) {
			VerticalLayoutContainer vlc1 = new VerticalLayoutContainer();
			VerticalLayoutContainer vlc2 = new VerticalLayoutContainer();
			
			vlc1.add(contentPanel.asWidget());
			vlc1.add(contentPanel.getButtonPanel());
			vlc2.add(personPanel.asWidget());
			TabPanel folderTabPanel = new TabPanel();
			folderTabPanel.add(vlc1, "Add Person");
			folderTabPanel.add(vlc2, "Persons");

			panel = new ContentPanel();
			panel.setHeading("Users Management");
			panel.add(folderTabPanel);

		}
		return panel;
	}
	
	
}
